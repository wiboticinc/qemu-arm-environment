FROM debian:bullseye
ENV DEBIAN_FRONTEND="noninteractive"

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils qemu-system-arm
RUN apt-get update && apt-get install -y --no-install-recommends build-essential gcc-8 g++-8
RUN apt-get update && apt-get install -y gcc-arm-none-eabi
RUN apt-get update && apt-get install -y --no-install-recommends vim nano

ENV QEMU_AUDIO_DRV none
ENTRYPOINT ["sleep", "infinity"]
